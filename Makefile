all: hello.wasm sxml.wasm counter.wasm todo.wasm realtime.wasm

hello.wasm: hello.scm
	guile hello.scm

sxml.wasm: sxml.scm
	guile sxml.scm

counter.wasm: counter.scm
	guile counter.scm

todo.wasm: todo.scm
	guile todo.scm

realtime.wasm: realtime_audio.scm realtime_webgl.scm
	guile realtime_audio.scm
	guile realtime_webgl.scm

serve:
	guile web-server.scm

clean:
	rm *.wasm
