const audioContext = new AudioContext();
const analyser = audioContext.createAnalyser();
analyser.fftSize = 2048
const analyserData = new Float32Array(2048);
const waveformData = new Float32Array(4096);

window.addEventListener("load", async function() {
    const canvas = document.querySelector("#glcanvas");
    const webgl = canvas.getContext("webgl");
    if (webgl === null) {
        return console.error("unable to init webgl");
    }

    const bindings = {};
    const skiplist = ["canvas", "drawingBufferWidth", "drawingBufferHeight"];
    for (const name of Object.getOwnPropertyNames(WebGLRenderingContext.prototype)) {
        if (skiplist.includes(name)) {
            continue;
        }
        const prop = WebGLRenderingContext.prototype[name];
        if (typeof(prop) === "function") {
            // e.g. {clearColor: WebGLRenderingContext.prototype.clearColor.bind(webgl),
            //            clear: WebGLRenderingContext.prototype.clear.bind(webgl)}
            bindings[name] = prop.bind(webgl);
        }
    }

    await Scheme.load_main("realtime_webgl.wasm", {}, {
        webgl: bindings,
        window: {
            requestAnimationFrame: requestAnimationFrame
        },
        analyser: {
            getFloatTimeDomainData() {
                // TODO not clear how best to share array here so process in js for now
                analyser.getFloatTimeDomainData(analyserData);
                for (let i = 0; i < 2048; i++) {
                    waveformData[i*2] = (i/2048)*4-1;
                    waveformData[i*2+1] = analyserData[i];
                }
                return waveformData;
            }
        }
    });

    let modules = {};
    modules["js-runtime/wtf8.wasm"] = await WebAssembly.compileStreaming(fetch("js-runtime/wtf8.wasm"));
    modules["js-runtime/reflect.wasm"] = await WebAssembly.compileStreaming(fetch("js-runtime/reflect.wasm"));
    modules["realtime_audio.wasm"] = await WebAssembly.compileStreaming(fetch("realtime_audio.wasm"));

    await audioContext.audioWorklet.addModule('realtime_worklet.js');
    const audioSink = new AudioWorkletNode(audioContext, 'audio-sink');
    audioSink.port.postMessage(modules);
	audioSink.connect(analyser);
    analyser.connect(audioContext.destination);
    document.querySelector("#toggleaudio").onclick = function() { audioContext.state === "running" ? audioContext.suspend() : audioContext.resume(); };
});
